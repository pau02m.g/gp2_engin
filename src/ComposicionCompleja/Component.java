package ComposicionCompleja;

public abstract class Component<T> {
	
	public GameObject goPadre;
	
	public Component(GameObject go) {
		this.goPadre = go;
	}
	
	
	public abstract void updateComponent();
	public abstract void renderComponent();
	
}




