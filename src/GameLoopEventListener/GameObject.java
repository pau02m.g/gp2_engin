package GameLoopEventListener;

public interface GameObject {
	public void update(double deltaTime);
	public void render();
	
	
}
