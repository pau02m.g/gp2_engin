package GameLoopEventListener;

public abstract class Component<T> implements Observer{
	
	// dejar claro que un componente de estos es como al unity un BoxColider2D o un SpriteRenderer pero el apartado 10 lo deja vastante inclaro
	
	public GameObject goPadre;
	
	public Component(GameObject go) {
		this.goPadre = go;
	}
	
	
	public abstract void updateComponent();
	public abstract void renderComponent();
}




