package GameLoopEventListener;

import java.util.Random;



public class TimeOut extends Component implements GameObject
{

	
	// achirripum
	public static boolean mostrar_cosas = true;
	
	
	Random r = new Random();
	Motor m = Motor.getInstance();
	
	
	private int tiempoDeVida;
	private double m_delta = 0;
	private int diferencia = -1;
	
	private Spawner Padre;

	
	
	
	public TimeOut(Spawner s) {
		super(s);
		System.out.println("Tiempo de vida del timer --> " + tiempoDeVida + " | creado |");
		Reset();
		Padre = s;
	}
	
	public TimeOut() {
		super(null);
		Reset();
	}
	
	
	
	
	@Override
	public void render() {
		/*
		if((int)m_delta != diferencia) {
			tiempoDeVida--;
			System.out.println("me queda --> " + tiempoDeVida + "s de vida");
			diferencia = (int) m_delta;
			if(tiempoDeVida <= 0) {
				System.out.println("me muero");
				m.removeGameObject(this);
			}
		}
		*/
		if(mostrar_cosas){
			System.out.println("Tiempo de vida del timer --> " + tiempoDeVida);
			mostrar_cosas = false;
		}
	
	}
	

	@Override
	public void update(double deltaTime) {
		m_delta += deltaTime;
		
		if(m_delta > tiempoDeVida) {
			System.out.println("me muero");
			Padre.getPool().Return(this);
			
			//m.removeGameObject(this);
			Reset();
			m_delta = 0;
			
			
			//
			
			mostrar_cosas = true;
			
		}
		this.updateComponent();
	}

	
	public void setTiempoDeVida(int tiempoDeVida) {
		this.tiempoDeVida = tiempoDeVida;
	}

	public void setPadre(Spawner s) {
		Padre = s;
	}
	
	public void Reset() {
		this.tiempoDeVida = r.nextInt(5)+1;
	}

	
	
	
	
	
	
	//
	
	
	
	
	@Override
	public void NotifyObserver(String nombre) {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	public void updateComponent() {
		
		//update();
	}

	@Override
	public void renderComponent() {
		// TODO Auto-generated method stub
		render();
	}



	
}
