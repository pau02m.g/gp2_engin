package GameLoopEventListener;

import java.util.ArrayList;

import ComposicionCompleja.Component;

public abstract class A_GameObject{

	private ArrayList<Component> ListaComponentes = new ArrayList<Component>();
	

	public void addComponent(Component element) {
		ListaComponentes.add(element);
	}
	
	public <T extends Component> void removeComponent(Class<T> element) {
	
		for (int i = ListaComponentes.size(); i > 0; i--) {
			if(element.isInstance(ListaComponentes.get(i-1))) {
				ListaComponentes.remove(ListaComponentes.get(i-1));
			}
		}
	}
	
	public <T extends Component> boolean hasComponent(Class<T> element) {
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {
				return true;
			}
		}
		return false;
	}
	
	
	public <T extends Component> Class<T> getComponent(Class<T> element) {
		
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {//element.isInstance(c) //c.getClass().equals(element.getClass())
				return element;
			}
		}
		return null;
	}
	
	public void updateComponent() {
		for(Component c : ListaComponentes) {
			c.updateComponent();
		}
	}
	public void renderComponent() {
		for(Component c : ListaComponentes) {
			c.renderComponent();
		}
	}
	
}
