package GameLoopEventListener;

public class Main {
	public static void main(String[] args) {
		Motor m = Motor.getInstance();
		
		m.init();
		
		boolean jugar = true;
		
		int fps = 60;
		
		int dormir = 1000/60;
		
		double deltaTime = 0;
		Long UltimoFrame = System.nanoTime();
		
		
		while(jugar) {
			deltaTime = (System.nanoTime() - UltimoFrame)/1000000000d;
			
			
			m.input();
			m.update(deltaTime);
			m.render();
			
			
			UltimoFrame = System.nanoTime();
			
			try {
				Thread.sleep(dormir);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
