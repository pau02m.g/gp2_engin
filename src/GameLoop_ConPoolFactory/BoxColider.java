package GameLoop_ConPoolFactory;

public class BoxColider extends Component{

	public BoxColider(GameObject go) {
		super(go);
		
	}

	@Override
	public void updateComponent() {
		System.out.println(this.getClass() + " colisiono con algo |update| " + this.getClass());
	}

	@Override
	public void renderComponent() {
		System.out.println(this.getClass() + " 'me pinto' |render| " + this.getClass());
	}

}
