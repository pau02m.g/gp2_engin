package GameLoop_ConPoolFactory;

public class TimeOutsFactory implements PoolFactory<TimeOut>{


	private static TimeOutsFactory me = null;
	
	private TimeOutsFactory() {
		
	}
	
	public static TimeOutsFactory instanciate() {
		if(me == null) 
			me = new TimeOutsFactory();
			
		return me;
	}
	

	// hacerla singleton
	
	
	@Override
	public TimeOut crear() {
		
		return new TimeOut();
	}

	@Override
	public void activate(TimeOut element) {
		element.Reset();
		Motor.getInstance().addGameObject(element);
	}

	@Override
	public void desactivate(TimeOut element) {
		Motor.getInstance().removeGameObject(element);
		
	}

	@Override
	public void delete(TimeOut element) {
		
	}


}
