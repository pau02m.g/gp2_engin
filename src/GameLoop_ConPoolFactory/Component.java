package GameLoop_ConPoolFactory;

public abstract class Component<T> {
	
	// dejar claro que un componente de estos es como al unity un BoxColider2D o un SpriteRenderer
	
	public GameObject goPadre;
	
	public Component(GameObject go) {
		this.goPadre = go;
	}
	
	
	public abstract void updateComponent();
	public abstract void renderComponent();
}




