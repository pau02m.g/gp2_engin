package GameLoop_ConPoolFactory;

public interface GameObject {
	public void update(double deltaTime);
	public void render();
	
	
}
