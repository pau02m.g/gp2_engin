package GameLoopArreglado;

public abstract class Component<T> implements Observer{
	
	
	
	private GameObject goPadre;
	


	public Component(GameObject go) {
		this.goPadre = go;
	}
	

	
	public void setPadre(GameObject go) {
		this.goPadre = go;
	}
	
	public GameObject getPadre() {
		return this.goPadre;
	}
	
	
	public abstract void updateComponent(double delta);
	public abstract void renderComponent();
}




