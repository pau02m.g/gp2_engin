package GameLoopArreglado;

import java.util.Random;

public class Spawner extends Component{

	// Variables
	Motor m = Motor.getInstance();
	
	double m_delta;
	
	private TimeOutsFactory m_timeout_factory = TimeOutsFactory.instanciate();
	private MyPoolG<TimeOut> pool;
	
	Random r = new Random();
	
	
	
	// Constructores
	public Spawner() {
		super(null);
		pool = new MyPoolG(m_timeout_factory); // hago la pool de Time's Out's // tengo que pasarle una factoria // estaria cabron que la factorya que le paso fuera de timeouts
		System.out.println("He creado un Spawner										| Spawner --> linea 23");
	}
	
	
	public Spawner(GameObject go) {
		super(go);
		pool = new MyPoolG(m_timeout_factory);
		
		for(TimeOut to : pool.getAllPool()) {
			to.setPadre(go);
		}
		
		System.out.println("He creado un Spawner con padre								| Spawner --> linea 35"); // llego
	}
	
	
	
	


	
	
	// Funcionamiento
	
	@Override
	public void renderComponent() {
		

	}
	

	@Override
	public void updateComponent(double deltaTime) {
		
		//System.out.println("updateComponent del spawner					| Spawner --> linea 53"); // llega
		
		m_delta += deltaTime;
		
		if(m_delta > 5) {
			System.out.println("Reclamo un timeout							| Spawner --> linea 58"); // llega

			TimeOut TO = (TimeOut) pool.get();
			System.out.println("Pillo un timeout							| Spawner --> linea 61");
			TO.setPadreSpawner(this);
			TO.setTiempoDeVida(r.nextInt(5)+1);
			
			
			
			//m.addGameObject(TO);
			m_delta = 0;
		}
	}

	
	@Override
	public void NotifyObserver(String nombre) {
		// TODO Auto-generated method stub
		
	}	
	
	
	
	// Get'ers y Set'ers
	
	public MyPoolG getPool() {
		return pool;
	}
}
