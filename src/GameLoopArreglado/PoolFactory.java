package GameLoopArreglado;

public interface PoolFactory<T> {

	public T crear();
	public void activate(T element);
	public void desactivate(T element);
	public void delete(T element);
}
