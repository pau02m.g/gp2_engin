package GameLoop_ConPool;

import java.util.ArrayList;
import java.util.Iterator;


public class MyPool<GameObject> {

	private  ArrayList<GameObject> Piscina = new ArrayList<GameObject>();
	private  ArrayList<Boolean> PiscinaBool = new ArrayList<Boolean>();

	

	public MyPool(GameObject o) {
		for (int i = 0; i < 10; i++) {
			Piscina.add(o);
			PiscinaBool.add(true);
		}
	}
	
	

	public GameObject get() {
		
		for (int i = 0; i < 10; i++) {
			if(PiscinaBool.get(i)) {
				PiscinaBool.set(i, false);
				return Piscina.get(i);
			}
		}
		return null;
	}
	
	
	public void returnToPool(GameObject element) {
		for (int i = 0; i < 10; i++) {
			
			if(PiscinaBool.get(i) == false && Piscina.get(i) == element) {
				PiscinaBool.set(i, true);
				return;
			}
		}
		
	}
	
	
	
	@Override
	public String toString() {
		String txt = "";
		for (int i = 0; i < 10; i++) {
			if(PiscinaBool.get(i)) {
				txt += Piscina.get(i).toString() + " ";
			}
			
		}
		return txt;
		
		
	}
	
	

	
}
