package GameLoop_ConPool;

import java.util.Random;

public class Spawner implements GameObject{

	Motor m = Motor.getInstance();
	
	double m_delta;
	private MyPool pool;
	
	Random r = new Random();
	
	public Spawner() {
		pool = new MyPool(new TimeOut(this)); // hago la pool de Time's Out's
	}
	
	public MyPool getPool() {
		return pool;
	}

	@Override
	public void render() {
		

	}
	

	@Override
	public void update(double deltaTime) {
		m_delta += deltaTime;
		
		if(m_delta > 5) {
			System.out.println("Creo un timeout");
			
			TimeOut TO = (TimeOut) pool.get();
			TO.setTiempoDeVida(r.nextInt(5)+1);
			
			m.addGameObject(TO);
			m_delta = 0;
		}
	}
	
	
	
	
	
}
