package GameLoop_ConPool;

import java.util.ArrayList;
import java.util.Iterator;



public class Motor {
	
	
	ArrayList<GameObject> m_GameObjects = new ArrayList<GameObject>();
	ArrayList<GameObject> m_GameObjectsSecundaria = new ArrayList<GameObject>();
	
	
	
	private static Motor m_motor = null;
	
	private Motor() {
		
	}
	
	public static Motor getInstance() {
		if(m_motor == null) 
			m_motor = new Motor();
		
		return m_motor;
	}
	
	

	
	
	public void init() {
		m_GameObjects.add(new Timer());
		m_GameObjects.add(new Spawner());
	}
	
	public void input() {
		
	}
	
	
	public void update(double deltaTime) {
	
		for (int i = m_GameObjects.size(); i > 0; i--) {
			m_GameObjects.get(i-1).update(deltaTime);
		}
		
	}
	
	public void render() {

		for (int i = m_GameObjects.size(); i > 0; i--) {
			m_GameObjects.get(i-1).render();
		}
	}
	
	
	
	public void addGameObject(GameObject go) {
		
		System.out.println("se a a�adido un go " + go.getClass());
		m_GameObjects.add(go);
	}
	
	
	
	
	public void removeGameObject(GameObject go) {
		
		
		System.out.println("se a borrado un go " + go.getClass());
		
		
		if(m_GameObjects.contains(go)) {
			m_GameObjects.remove(go);
		}
	}

	
	
	
	
	
	
}
