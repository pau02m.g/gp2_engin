package GameLoop_ConPool;

import java.util.Random;



public class TimeOut implements GameObject
{

	
	Random r = new Random();
	Motor m = Motor.getInstance();
	
	
	private int tiempoDeVida;
	private double m_delta = 0;
	private int diferencia = -1;
	
	private Spawner Padre;

	
	
	public TimeOut(Spawner s) {
		System.out.println("Tiempo de vida del timer --> " + tiempoDeVida);
		Padre = s;
	}
	
	
	@Override
	public void render() {
		/*
		if((int)m_delta != diferencia) {
			tiempoDeVida--;
			System.out.println("me queda --> " + tiempoDeVida + "s de vida");
			diferencia = (int) m_delta;
			if(tiempoDeVida <= 0) {
				System.out.println("me muero");
				m.removeGameObject(this);
			}
		}
		*/
		System.out.println("Tiempo de vida del timer --> " + tiempoDeVida);
	}
	

	@Override
	public void update(double deltaTime) {
		m_delta += deltaTime;
		
		if(m_delta > tiempoDeVida) {
			System.out.println("me muero");
			Padre.getPool().returnToPool(this);
			m.removeGameObject(this);
			m_delta = 0;
		}
	}

	
	public void setTiempoDeVida(int tiempoDeVida) {
		this.tiempoDeVida = tiempoDeVida;
	}



}
