package GameLoopEstados;

import java.util.ArrayList;
import java.util.Random;



public class RETimeOut extends Component{

	// achirripum
	private boolean mostrar_cosas = true;
	
	
	// Variables
	
	ArrayList<StateStack> MisEstadosPosibles;
	MyStack<StateStack> StackState_MyStateAcc;
	
	
	Random r = new Random();
	Motor m = Motor.getInstance();
	
	private int tiempoDeVida;
	private double m_delta = 0;
	private int diferencia = -1;
	
	private Spawner Padre;


	private String nombre;
	
	
	
	// Constructores
	
	public RETimeOut() {
		super(null);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		Reset();
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 46");
	}
	
	public RETimeOut(Spawner s, GameObject go) {
		super(go);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		Reset();
		
		Padre = s;
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 61");
	}
	
	public RETimeOut(Spawner s) {
		super(null);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		Reset();
		
		Padre = s;
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 76");
	}
	
	public RETimeOut(GameObject go) {
		super(go);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		Reset();
		
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 90");
	}
	
	public RETimeOut(String n) {
		super(null);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		Reset();
		this.nombre = n;
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 104");
	}
	
	public RETimeOut(GameObject go, String n) {
		super(go);
		this.MisEstadosPosibles  = new ArrayList<StateStack>();
		this.StackState_MyStateAcc  = new MyStack<StateStack>();
		
		this.nombre = n;
		Reset();
		
		
		EstablecerEstadoPrincipal();
		EventListener.getInstance().RegisterObserver("RES", this);
		
		System.out.println("He creado un RETimeOut 									| RETimeOut --> linea 119 | Tiempo de vida: " + tiempoDeVida); // llego
	}
	
	
	
	
	
	// Funcionamiento
	public void addState(StateStack state) {
		for(StateStack s : MisEstadosPosibles) {
			if(s.getClass() == state.getClass()) {
				return;
			}
		}
		MisEstadosPosibles.add(state);
	}
	
	public ArrayList<StateStack> getMisEstadosPosibles() {
		return MisEstadosPosibles;
	}
	
	public void removeState(StateStack state) {
		MisEstadosPosibles.remove(state);
	}
	
	public  <T extends StateStack> void pushState(Class<T> state) {
		for(StateStack s : MisEstadosPosibles) {
			if(state.isInstance(s)) {
				StackState_MyStateAcc.Push(s);
			}
		}
	}
	
	public  <T extends StateStack> StateStack popState() {
		StateStack a = StackState_MyStateAcc.Pop();
		if(a != null) {
			a.onExit();	
		}
		if(StackState_MyStateAcc.peek() != null) {
			StackState_MyStateAcc.peek().onEnter();
		}
		return a;
	}
	
	public void VoidpopState() {
		StateStack a = StackState_MyStateAcc.Pop();
		a.onExit();
		StackState_MyStateAcc.peek().onEnter();
	}
	
	public <T extends StateStack> void setState(Class<T> state) {
		StateStack a = StackState_MyStateAcc.Pop();
	
		for(StateStack s : MisEstadosPosibles) {
			if(state.isInstance(s)) {
				if(a != null) {
					System.out.print(this.nombre + "                  -->				");
					a.onExit();
				}
				StackState_MyStateAcc.Push(s);
				System.out.print(this.nombre + "                  -->				");
				s.onEnter();
			}
		}
	}
	

	
	@Override
	public void renderComponent() {
		// TODO Auto-generated method stub
		if(mostrar_cosas){
			System.out.println("Tiempo de vida del retimer --> " + tiempoDeVida +"			| RETimeOut --> linea 164");
			mostrar_cosas = false;
		}
	}


	
	@Override
	public void updateComponent(double deltaTime) {
		
		m_delta += deltaTime;
		
		
		if(m_delta > tiempoDeVida && Vida.class.isInstance(StackState_MyStateAcc.peek())) { // me muero
			System.out.println("me muero						| TimeOut --> linea 177");
			
			EventListener.getInstance().NotifyObservers("DEAD", this.nombre); 
			//Padre.getPool().Return(this); // no entiendo nada el ejercicio la verdad
			Reset();
			
			
			//this.StackState_MyStateAcc.peek().onExit(); // sale la que tenia
			this.setState(Mort.class);
			//this.StackState_MyStateAcc.peek().onEnter(); // entra la nueva
			
			m_delta = 0;
			
			//
			mostrar_cosas = true;
		}
		
		else if(m_delta > 3 && Mort.class.isInstance(StackState_MyStateAcc.peek())) {
			
			
			//this.StackState_MyStateAcc.peek().onExit();
			this.setState(Ressurreccio.class);
			//this.StackState_MyStateAcc.peek().onEnter();
			EventListener.getInstance().NotifyObservers("RES", this.nombre);
			
			m_delta = 0;
		}
	}


	
	
	@Override
	public void NotifyObserver(String event, String name) {
		
		if(event.equals("RES") && name.equals(this.nombre)) {
			if(Ressurreccio.class.isInstance(StackState_MyStateAcc.peek())) {
				//StackState_MyStateAcc.peek().onExit();
				this.setState(Vida.class);
				//StackState_MyStateAcc.peek().onEnter();
			}
		}
		
	}
	
	
	
	
	// Funciones
	
	public void EstablecerEstadoPrincipal() {
		this.addState(new Vida());
		this.addState(new Ressurreccio());
		this.addState(new Mort());
		
		this.setState(Vida.class);
	}
	
	public void Reset() {
		this.tiempoDeVida = r.nextInt(5)+1;
		
	}
	
	
	// Get'ers y Set'ers
	
	public void setTiempoDeVida(int tiempoDeVida) {
		this.tiempoDeVida = tiempoDeVida;
	}

	public void setPadreSpawner(Spawner s) {
		Padre = s;
	}
	
}
