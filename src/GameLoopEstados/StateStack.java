package GameLoopEstados;

public interface StateStack<T> {

	public void onEnter();
	public void onExit();
		
}
