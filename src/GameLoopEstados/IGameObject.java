package GameLoopEstados;

public interface IGameObject {
	public void update(double deltaTime);
	public void render();
	
}
