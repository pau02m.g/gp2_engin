package GameLoopEstados;

import java.util.ArrayList;
import java.util.Iterator;



public class Motor { // el motor es el gamelup
	
	
	ArrayList<GameObject> m_GameObjects = new ArrayList<GameObject>();
	ArrayList<GameObject> m_GameObjectsSecundaria = new ArrayList<GameObject>();
	
	private EventListener EL = EventListener.getInstance();
	
	
	private static Motor m_motor = null;
	
	private Motor() {
		
	}
	
	public static Motor getInstance() {
		if(m_motor == null) 
			m_motor = new Motor();
		
		return m_motor;
	}
	
	


	public void init() {
		
		m_GameObjects.add(new Timer());
		
		GameObject go1 = new GameObject("test01");
		GameObject go2 = new GameObject("test01");
		GameObject go3 = new GameObject("test01");
		
		go1.addComponent(new RETimeOut(go1, "TimeOut"));
		go2.addComponent(new RETimeOut(go1, "TimeOut(1)"));
		go3.addComponent(new RETimeOut(go1, "TimeOut(2)"));
		
		m_GameObjects.add(go1);
		m_GameObjects.add(go2);
		m_GameObjects.add(go3);
		
		
		
		/*
		Spawner spw = new Spawner(goPrincipal, TimeOutsFactory.instanciate());
		goPrincipal.addComponent(spw);
		m_GameObjects.add(goPrincipal);
		
		
		for(int i = 0;i<10;i++) {
			GameObject go = new GameObject("testEvent's"+i);
			go.addComponent(new CRespects(go));
		}
		*/
		

		System.out.println("Me inicio             									| Motor --> linea 58" );
	}
	
	public void input() {
		
	}
	
	
	public void update(double deltaTime) {
		//System.out.println("update del motor										| Motor --> linea 53"); 			// llega
		
		for (int i = m_GameObjects.size(); i > 0; i--) {
			m_GameObjects.get(i-1).update(deltaTime);
		}
		
	}
	
	public void render() {

		for (int i = m_GameObjects.size(); i > 0; i--) {
			m_GameObjects.get(i-1).render();
		}
	}
	
	
	
	public void addGameObject(GameObject go) {
		
		System.out.println("se a a�adido un go " + go.getClass());
		m_GameObjects.add(go);
	}
	
	
	
	
	public void removeGameObject(IGameObject go) {
		
		
		System.out.println("se a borrado un go " + go.getClass());
		
		
		if(m_GameObjects.contains(go)) {
			m_GameObjects.remove(go);
		}
	}

	
	
	
	
	
	
}
