package GameLoopEstados;

import java.io.IOException;
import java.util.ArrayList;

public class MyStack<T> {

	
	private ArrayList<T> pila;
	
	public MyStack() {
		pila = new ArrayList<T>();
	}

	public T Pop() {
	
		if(pila.size()<=0) {
			return null;
		}
		else {
			T holder = pila.get(pila.size()-1);
			pila.remove(pila.size()-1);
			return holder;
		}
		
	}
	
	public void Push (T element) {	
	    pila.add(element);
	}
	
	public T peek() {
		if(pila.size()<=0) {
			return null;
		}
		
		return pila.get(pila.size()-1);
	}
	
	
	@Override
	public String toString() {
		return "Contenido de la Pila --> " + pila;
	}
	
}
