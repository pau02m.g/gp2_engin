package GameLoopEstados;

public interface Observer{

	public void NotifyObserver(String event, String name);
	
}
