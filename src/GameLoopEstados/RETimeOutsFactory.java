package GameLoopEstados;

public class RETimeOutsFactory implements PoolFactory<RETimeOut>{


	private static RETimeOutsFactory me = null;
	
	private RETimeOutsFactory() {
		
	}
	
	public static RETimeOutsFactory instanciate() {
		if(me == null) 
			me = new RETimeOutsFactory();
			
		return me;
	}
	

	private static int id = 0;
	
	
	@Override
	public RETimeOut crear() {
		RETimeOut RT = new RETimeOut("RETimeOut("+id+")");
		id++;
		return RT;
	}

	@Override
	public void activate(RETimeOut element) {
		element.Reset();
		
		System.out.println("Reseteo el elemento TimeOut 								| TimeOutsFactory --> linea 32");
		element.getPadre().addComponent(element);
		System.out.println("A�ado el componente TimeOut 								| TimeOutsFactory --> linea 32");
		
	}

	@Override
	public void desactivate(RETimeOut element) {
		element.getPadre().removeComponent(TimeOut.class); // como se que es la factoria de TimeOuts pues eso TimeOut.class y tal
		//Motor.getInstance().removeGameObject(element);
		
	}

	@Override
	public void delete(RETimeOut element) {
		
	}


}
