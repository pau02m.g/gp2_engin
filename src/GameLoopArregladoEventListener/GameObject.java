package GameLoopArregladoEventListener;

import java.util.ArrayList;



public class GameObject implements IGameObject{

	// Variables
	private ArrayList<Component> ListaComponentes = new ArrayList<Component>();
	private String nombre;
	
	
	// Constructores
	
	public GameObject() {
		
	}
	
	
	public GameObject(String s) {
		this.nombre = s;
	}
	
	

	// Funcionamiento
	public void update(double deltaTime) {
		//System.out.println("Update del gameObject					| GameObject --> linea 30"); // llega
		this.updateComponents(deltaTime);
		
	}
	
	public void render() {
		this.renderComponents();
	}
	
	
	public void addComponent(Component element) {
		ListaComponentes.add(element);
		
		System.out.println("Se a a�adido un elemento tipo " + element.getClass() + "  (n� de componentes --> " + this.ListaComponentes.size() + ") 		| GameObject --> linea 42");
	}
	
	public <T extends Component> void removeComponent(Class<T> element) {
	
		for (int i = ListaComponentes.size(); i > 0; i--) {
			if(element.isInstance(ListaComponentes.get(i-1))) {
				ListaComponentes.remove(ListaComponentes.get(i-1));
				System.out.println("Se a borrado un elemento tipo " + element + "  (n� de componentes --> " + this.ListaComponentes.size() + ") 		| GameObject --> linea 50");
				
				return; // solo quito 1 
				
			}
		}
	}
	
	public <T extends Component> boolean hasComponent(Class<T> element) {
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {
				return true;
			}
		}
		return false;
	}
	
	
	public <T extends Component> Class<T> getComponent(Class<T> element) {
		
		for(Component c : ListaComponentes) {
			if(element.isInstance(c)) {//element.isInstance(c) //c.getClass().equals(element.getClass())
				return element;
			}
		}
		return null;
	}
	
	public void updateComponents(double deltaTime) {
		//System.out.println("UpdateComponents del gameobject					| GameObject --> linea 76"); // llega
		for(int i = ListaComponentes.size(); i > 0; i--) {
			ListaComponentes.get(i-1).updateComponent(deltaTime);
		}
	}
	public void renderComponents() {
		for(int i = ListaComponentes.size(); i > 0; i--) {
			ListaComponentes.get(i-1).renderComponent();
		}
	}
	
	
	//Get'ers y Set'ers
	
	public void setNomnre(String s) {
		this.nombre = s;
	}
	public String getNombre() {
		return this.nombre;
	}
	
}
