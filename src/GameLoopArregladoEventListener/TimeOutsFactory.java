package GameLoopArregladoEventListener;

public class TimeOutsFactory implements PoolFactory<TimeOut>{


	private static TimeOutsFactory me = null;
	
	private TimeOutsFactory() {
		
	}
	
	public static TimeOutsFactory instanciate() {
		if(me == null) 
			me = new TimeOutsFactory();
			
		return me;
	}
	

	// hacerla singleton
	
	
	@Override
	public TimeOut crear() {
		
		return new TimeOut();
	}

	@Override
	public void activate(TimeOut element) {
		element.Reset();
		
		System.out.println("Reseteo el elemento TimeOut 								| TimeOutsFactory --> linea 32");
		element.getPadre().addComponent(element);
		System.out.println("A�ado el componente TimeOut 								| TimeOutsFactory --> linea 32");
		
	}

	@Override
	public void desactivate(TimeOut element) {
		element.getPadre().removeComponent(TimeOut.class); // como se que es la factoria de TimeOuts pues eso TimeOut.class y tal
		//Motor.getInstance().removeGameObject(element);
		
	}

	@Override
	public void delete(TimeOut element) {
		
	}


}
