package GameLoopArregladoEventListener;

import java.util.Random;



public class TimeOut extends Component
{

	// achirripum
	private static boolean mostrar_cosas = true;
	
	// Variables
	
	Random r = new Random();
	Motor m = Motor.getInstance();
	
	private int tiempoDeVida;
	private double m_delta = 0;
	private int diferencia = -1;
	
	private Spawner Padre;


	private String nombre;
	
	private int r_id = 55;
	
	// Constructores
	
	public TimeOut() {
		super(null);
		Reset();
		this.nombre = "Timeout("+r_id+")";

		System.out.println("He creado un TimeOut 									| TimeOut --> linea 36");
	}
	
	
	public TimeOut(Spawner s, GameObject go) {
		super(go);
		//System.out.println("Tiempo de vida del timer --> " + tiempoDeVida + " | creado |");
		Reset();
		Padre = s;
		
		System.out.println("He creado un TimeOut con padre y spawner asociado		| TimeOut --> linea 46");
	}
	
	public TimeOut(Spawner s) {
		super(null);
		Reset();
		Padre = s;
		
		System.out.println("He creado un TimeOut con spawner asociado				| TimeOut --> linea 54");
	}
	
	public TimeOut(GameObject go) {
		super(go);
		Reset();
		
		System.out.println("He creado un TimeOut con padre								| TimeOut --> linea 61");
	}
	
	
	
	// Funcionamiento
	
	@Override
	public void renderComponent() {
		/*
		if((int)m_delta != diferencia) {
			tiempoDeVida--;
			System.out.println("me queda --> " + tiempoDeVida + "s de vida");
			diferencia = (int) m_delta;
			if(tiempoDeVida <= 0) {
				System.out.println("me muero");
				m.removeGameObject(this);
			}
		}
		*/
		if(mostrar_cosas){
			System.out.println("Tiempo de vida del timer --> " + tiempoDeVida +"			| TimeOut --> linea 80");
			mostrar_cosas = false;
		}
	
	}
	

	@Override
	public void updateComponent(double deltaTime) {
		
		
		
		m_delta += deltaTime;
		
		if(m_delta > tiempoDeVida) {
			System.out.println("me muero						| TimeOut --> linea 95");
			
			EventListener.getInstance().NotifyObservers("DEAD", this.nombre); // cuando me muero notifico al evento DEAD con el nombre del TimeOut ()
			
			Padre.getPool().Return(this);
			
			//m.removeGameObject(this);
			Reset();
			m_delta = 0;
			
			
			//

			mostrar_cosas = true;
		}
	
	}

	@Override
	public void NotifyObserver(String event, String name) {
		// TODO Auto-generated method stub
		
	}
	
	// Get'ers y Set'ers
	
	public void setTiempoDeVida(int tiempoDeVida) {
		this.tiempoDeVida = tiempoDeVida;
	}

	public void setPadreSpawner(Spawner s) {
		Padre = s;
	}
	
	public void Reset() {
		this.tiempoDeVida = r.nextInt(5)+1;
		this.r_id = r.nextInt(500)+1;	
	}

	
	
}
