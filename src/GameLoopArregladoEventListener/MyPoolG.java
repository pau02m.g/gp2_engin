package GameLoopArregladoEventListener;

import java.util.ArrayList;
import java.util.Iterator;



public class MyPoolG<T> {
	
	private ArrayList<T> m_lista;
	private ArrayList<Boolean> m_bool;
	private PoolFactory<T> m_factoria; // es un poco raro teniendo en cuenta que esto es una interfaz
	
	public MyPoolG(PoolFactory<T> factoria) {
		
		m_lista = new ArrayList<T>();
		m_bool = new ArrayList<Boolean>();
		m_factoria = factoria;
		
		for (int i = 0; i < 10; i++) {  
			
			T elemento = m_factoria.crear(); 
			m_lista.add(elemento); 
			m_bool.add(true);
		}
	} 
	
	
	
	public T get(){
		
		for (int i = 0; i < 10; i++) {
			if(m_bool.get(i)) {
				m_bool.set(i, false);
				T element = m_lista.get(i); 	// pillo el elemento
				System.out.println("Pillo un elemento de la lista 							| MyPoolG --> linea 36");
				m_factoria.activate(element); 	// lo activo
				System.out.println("Activo el elemento que he pillado 						| MyPoolG --> linea 38");
				return element; 				// lo return
			}
			
		}
		return null;
		
	}
	
	
	
	public void Return (T elemento) {
		for (int i = 0; i < 10; i++) {
			if(m_lista.get(i) == elemento && m_bool.get(i) == false) { 	// si el que le doy existe y esta activado
				m_bool.set(i, true);									// lo pongo a true (por lo tanto lo podemos pillar)
				T element = m_lista.get(i);								// lo pillo
				m_factoria.desactivate(element);						// lo desactivo
				return; 
			}
		}
		
	}



	@Override
	public String toString() {
		String txt = "";
		for (int i = 0; i < 10; i++) {
			if(m_bool.get(i)) {
				txt += m_lista.get(i).toString() + " ";
			}
		}
		return txt;
	}
	
	
	// los timeouts no tienen padre
	
	public ArrayList<T> getAllPool(){
		return m_lista;
	}
	
}
